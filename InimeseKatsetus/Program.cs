﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InimeseKatsetus
{
    enum Sugu { Teadmata = 99, Mees = 1, Naine = 2 }
    class Inimene
    {
        private static Dictionary<int, Inimene> _Inimesed = new Dictionary<int, Inimene>();
        private static int _Loendur = 0;

        private int _Nr = ++_Loendur;
        private string _Nimi = "polenime";
        private DateTime _SünniAeg = DateTime.Now.Date;
        private Sugu _Sugu = Sugu.Teadmata;

        // järgnevad kolm on readonly propertyd (muuta saab vaid 
        // klassi sees)
        private Inimene _Kaasa = null;
        public Inimene Kaasa { get => _Kaasa; private set => _Kaasa = value; }

        // kommnetaar
        // ema ja isa puhul - sisemine meetod peab andma väärtusi kujul
        // _Ema = ... ja _Isa = ...
        // kaasa puhul lisasin private setteri - see võimaldab väärtusi omistada
        // Kaasa = ... (ei pea "nööri" kasutama, aga ikkagi on privaatne)

        public void Abielu(Inimene kaasa)
        {
            if (this.Kaasa == null && kaasa.Kaasa == null)
                if (this.Vanus > 15 && kaasa.Vanus > 15)  // lisandunud ärireegel
                    if (this.Sugu != Sugu.Teadmata 
                        && kaasa.Sugu != Sugu.Teadmata 
                        && this.Sugu != kaasa.Sugu)
            {
                this.Kaasa = kaasa;
                kaasa.Kaasa = this;
            }
        }

        public void Lahutus()
        {
            if (this.Kaasa != null)
            {
                this.Kaasa.Kaasa = null;
                this.Kaasa = null;
            }
        }

        public void LisaLaps(Inimene laps)
        {
            // meil on kolm inimest
            // this (see, kellele lapse lisame)
            // this.Kaasa (see, kellele ma samuti lapse lisame)
            // laps (see keda me lapseks lisame)
            // meil on vaja:
            //      this laste hulka lisada laps
            //      this Kaasa (kui this ikka on abielus) laste hulka lisada laps
            //      lapsele märkida isa või ema - this
            //      lapsele märkida ema või isa - this.Kaasa (kui selline ikka on)

            // esteks kontrollime, et laps ikka on
            if (laps != null)
            {
                this._Lapsed.Add(laps);
                this.Kaasa?._Lapsed.Add(laps); // see küsimärk on seal miks?
                if (this.Sugu == Sugu.Mees)
                    { laps._Isa = this; laps._Ema = this.Kaasa; }
                else 
                if (this.Sugu == Sugu.Naine)
                    { laps._Isa = this.Kaasa; laps._Ema = this; }
            }
        }

        private Inimene _Ema = null;
        public Inimene Ema { get => _Ema; }

        private Inimene _Isa = null;
        public Inimene Isa { get => _Isa; }

        // mõtle välja, miks seal ToList() on ??? 
        private List<Inimene> _Lapsed = new List<Inimene>();
        public List<Inimene> Lapsed { get => _Lapsed.ToList(); }


        //public Inimene() { }

        public Inimene(string nimi = "polenime", Sugu sugu = Sugu.Teadmata, DateTime? sünniaeg = null)
        {
            this.Nimi = nimi;
            this.Sugu = sugu;

            //if (sünniaeg.HasValue) this.SünniAeg = sünniaeg.Value;
            // else this.SünniAeg = DateTime.Now.Date;

            this.SünniAeg = sünniaeg ?? DateTime.Now.Date;

            _Inimesed.Add(this._Nr, this);

        }

        public static List<Inimene> Inimesed() => _Inimesed.Values.ToList();

        //public static List<Inimene> Inimesed()
        //{
        //    return _Inimesed.Values.ToList();
        //}

        public static Inimene ByNr(int nr) => _Inimesed.Keys.Contains(nr) ?  _Inimesed[nr] : null;
        
        //public static Inimene ByNr(int nr)
        //{
        //    return _Inimesed[nr];
        //}

        public string Nimi
        {
            get => _Nimi;
            set => _Nimi = 
                    value.Length == 0 
                    ? _Nimi 
                    : value.Substring(0, 1).ToUpper() + value.Substring(1).ToLower();
        }


        public DateTime SünniAeg
        {
            get => _SünniAeg;
            set => _SünniAeg = value > DateTime.Now 
                ? DateTime.Now.Date : value.Date;
        }

        public String SSünniAeg
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime d))
                {
                    SünniAeg = d;
                }
            }
        }

        public Sugu Sugu
        {
            //get => _Sugu;
            get
            {
                return _Sugu;
            }
            //set => _Sugu = _Sugu == 0 ? value : _Sugu;
            set
            {
                if (_Sugu == Sugu.Teadmata) _Sugu = value; //else _Sugu = _Sugu;
            }
        }

        public int Vanus
        {
            get
            {
                int v = DateTime.Now.Year - _SünniAeg.Year;
                // v += _SünniAeg.AddYears(v) > DateTime.Now ? -1 : 0;
                if (_SünniAeg.AddYears(v) > DateTime.Now) v = v - 1;
                return v;
            }
        }

        // teeme siis veel
        // 3A. meetod trükilapsed
        public void TrykiLapsed()
        {
            Console.WriteLine(LeiaLapsed);
        }

        public string LeiaLapsed
        {
            get
            {
                // siin peaks siis trükkima välja 
                if (this._Lapsed.Count == 0) return "";
                string vastus = $"\n{Nimi} lapsed on:";
                foreach (var x in _Lapsed)
                {
                    vastus += $"\n\t{x.LapseLiik} {x.Nimi}";
                }
                return vastus;
            }
        }

        string LapseLiik
        {
            get
            {
                switch (this._Sugu)
                {
                    case Sugu.Mees:
                        return "poeg";
                    case Sugu.Naine:
                        return "tütar";
                    default:
                        return "laps";
                }
            }
        }

        /*
         * 4. Lisame funktsiooni public Inimene UusLaps(Nimi, Sugu)
	        * lapsel vanemad
	        * vanema(te)l laps lisaks
	        * lapse sünniaeg täna (või kolmas parameeter)
         */

        public Inimene UusLaps(string nimi, Sugu sugu)
        {
            Inimene uus = new Inimene() { Nimi = nimi, Sugu = sugu };
            this.LisaLaps(uus);
            return uus;
        }

        public override string ToString()
        {
            return $"{_Nr}. {Sugu} {_Nimi} (sündinud {_SünniAeg:dd.MMMM.yyyy}) vanusega {Vanus} { (Kaasa == null ? "" : $"(abielus: { Kaasa.Nimi}-ga)") } {LeiaLapsed}";
        }
    }
    class Program 
    {
        static void Main(string[] args)
        {
            //Inimene henn = new Inimene { Sugu = Sugu.Mees, Nimi = "henn", SSünniAeg = "1955/03/07" };

            Inimene henn = new Inimene
            {
                Sugu = Sugu.Mees,
                Nimi = "henn",
                SSünniAeg = "1955/03/07"
            };

            Inimene a1 = new Inimene(sugu: Sugu.Teadmata, nimi: "Ants");


            Inimene ants = new Inimene { Sugu = Sugu.Mees, Nimi = "aNTS", SSünniAeg = "2055/03/07" };
            Inimene paul = new Inimene { Nimi = "PAUL", SSünniAeg = "1955/03/07 10:00" };
            Inimene xxxx = new Inimene { SSünniAeg = "1955/03/07" };
            Inimene syndimata = new Inimene();
            Console.WriteLine(syndimata.SünniAeg);

            Console.WriteLine(henn);
            Console.WriteLine(ants);
            Console.WriteLine(paul);
            Console.WriteLine(xxxx);
            henn.Sugu = Sugu.Naine;
            Console.WriteLine(henn);
            paul.Sugu = Sugu.Mees;
            Console.WriteLine(paul);
            paul.Nimi = "";
            Console.WriteLine(paul);
            Console.WriteLine(paul.SünniAeg.ToShortDateString());

            Console.WriteLine(Inimene.ByNr(28)?.ToString()??"sellist meil ei ole");

            Inimene juku = new Inimene { Nimi = "Juku", SSünniAeg = "1.1.2000", Sugu = Sugu.Mees };
            Inimene manni = new Inimene { Nimi = "Manni", SSünniAeg = "7.7.2000", Sugu = Sugu.Naine };

            juku.LisaLaps(new Inimene { Nimi = "miku", Sugu = Sugu.Mees });
            juku.Abielu(manni);
            Console.WriteLine(juku);
            Console.WriteLine(manni);

            Console.WriteLine(new Inimene { Nimi = "Tanel", Sugu = Sugu.Mees, SSünniAeg = "1982/11/12" });

            juku.LisaLaps(new Inimene { Nimi = "juss", Sugu = Sugu.Mees });
            juku.Lahutus();
            juku.Abielu(new Inimene { Nimi = "malle", Sugu = Sugu.Naine, SSünniAeg = "1990/01/01" });
            juku.LisaLaps(new Inimene { Nimi = "pipi", Sugu = Sugu.Naine });

            foreach(var x in juku.Lapsed)
                Console.WriteLine($"{(x.Sugu == Sugu.Mees ? "poeg" : "tütar")} {x.Nimi} vanemad {x.Isa?.Nimi} ja {x.Ema?.Nimi}");

            juku.TrykiLapsed();

            Console.WriteLine(juku.UusLaps("pisike", Sugu.Naine));

            juku.TrykiLapsed();

            foreach (var x in juku.Lapsed)
                Console.WriteLine($"{(x.Sugu == Sugu.Mees ? "poeg" : "tütar")} {x.Nimi} vanemad {x.Isa?.Nimi} ja {x.Ema?.Nimi}");


        }
    }
}
